//
//  Music.swift
//  musicBox
//
//  Created by Vadim on 14.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit

class Audio: NSObject {
    
    var id: String?
    var audioName: String?
    var audioUrl: String?
    
    override init() {
        self.id = ""
        self.audioName = ""
        self.audioUrl = ""
    }
    
    init(dictionary: [String: Any]) {
        self.id = dictionary["id"] as? String
        self.audioName = dictionary["audioName"] as? String
        self.audioUrl = dictionary["audioUrl"] as? String
    }
    
}
