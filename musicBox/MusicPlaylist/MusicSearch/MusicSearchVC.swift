//
//  MusicSearchVC.swift
//  musicBox
//
//  Created by Vadim on 14.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit
import Firebase

class MusicSearchVC: UIViewController {
    
    let cellId = "cellId"
    
    var categoryName = ""
    var subCategoryName = ""
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.separatorStyle = .none
        tv.backgroundColor = UIColor.clear
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    var musicList = [Audio]()
    
    var parsedDictionary = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = customContentBlueColor
        navigationItem.title = "Аудио"

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MusicSearchCell.self, forCellReuseIdentifier: cellId)
        
        view.addSubview(tableView)
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
     
        fetchMusicList()
    }
    
    func fetchMusicList() {
        
//        musicList = []
//        tableView.reloadData()
//
//        let refref = Database.database().reference().child("music-category").child(subCategoryName)
//        refref.observeSingleEvent(of: .value) { (snapshot) in
//
//            guard let dictionary = snapshot.value as? [String: AnyObject] else {
//                return
//            }

            for key in parsedDictionary {
                
                self.addMusic(audioId: key, totalCount: parsedDictionary.count)
                
            }
            
            
//        }
       
        
    }
    
    func addMusic(audioId: String, totalCount: Int) {
        
        let ref = Database.database().reference().child("music").child(audioId)
        
        ref.keepSynced(true)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            
            let music = Audio(dictionary: dictionary)
        
            music.id = audioId
            
            self.musicList.append(music)
            
            if self.musicList.count == totalCount {
                self.tableView.reloadData()
            }
                
            
            
        }
        
    }
    
    func showAlertWithMessage(message: String) {
        
        let alertController = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "ОК", style: .default, handler: { (action) in
            
            
            
        }))
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    func selectedItem(indexRow: Int) {
        
        if let sessionId = UserDefaults.standard.string(forKey: sessionIDKey) {
            
            if let musicId = musicList[indexRow].id {
            Database.database().reference().child("playlists").child(sessionId).child(categoryName).updateChildValues([musicId: 1])
                
                showAlertWithMessage(message: "Аудио успешно добавлено!")
            }

        }
        
    }
    
}

extension MusicSearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musicList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MusicSearchCell
        cell.musicTextLabel.text = musicList[indexPath.row].audioName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedItem(indexRow: indexPath.row)
        
    }
    
}
