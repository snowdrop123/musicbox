//
//  MusicCategoriesSearchVC.swift
//  musicBox
//
//  Created by Vadim on 19.10.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit
import Firebase

class MusicCategoriesSearchVC: UIViewController {
    
    var categoryName = ""
    
    let cellId = "cellId"
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.separatorStyle = .none
        tv.backgroundColor = UIColor.clear
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    var categories = [String]()
    
    var parsedDictionary = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Категории"
        
        navigationItem.largeTitleDisplayMode = .never
                
        view.backgroundColor = customContentBlueColor
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MusicSearchCell.self, forCellReuseIdentifier: cellId)
        
        view.addSubview(tableView)
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
        
        if parsedDictionary.count == 0 {
            
            fetchMusicList()

        }
        
    }

    func fetchMusicList() {
        
        categories = []
        tableView.reloadData()
        
        let ref = Database.database().reference().child("music-category")
        
        ref.keepSynced(true)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            
            self.parsedDictionary = dictionary
            
            var count = 0
            
            for key in dictionary.keys {
                
                print("key \(key)")
                self.categories.append(key)
                
                count += 1
                
                if count == dictionary.keys.count {
                    self.tableView.reloadData()
                }
                
            }
            
        }
        
    }
    

    
}

extension MusicCategoriesSearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MusicSearchCell
        cell.musicTextLabel.text = categories[indexPath.row]
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        print("parsedDictionary \(parsedDictionary)")
        
        guard let newDic = parsedDictionary[categories[indexPath.row]] as? [String: AnyObject] else {
            
            
            return
        }
        
        var testbkb = [String]()
        
        for key in newDic.keys {
            testbkb.append(key)
        }
        
        if let firstItem = testbkb.first, let value = newDic[firstItem] as? Int, value == 1 {
            let nextVC = MusicSearchVC()
            nextVC.categoryName = categoryName
            nextVC.subCategoryName = categories[indexPath.row]
            nextVC.parsedDictionary = testbkb
            navigationController?.pushViewController(nextVC, animated: true)
        } else {
           
            let nextVC = MusicCategoriesSearchVC()
            nextVC.categories = testbkb
            nextVC.parsedDictionary = newDic
            navigationController?.pushViewController(nextVC, animated: true)

        }
        

 
        print("parsedDictionary[categories[indexPath.row]] \(newDic)")
        
        
        
//        let nextVC = MusicSearchVC()
//        nextVC.categoryName = categoryName
//        nextVC.subCategoryName = categories[indexPath.row]
//        navigationController?.pushViewController(nextVC, animated: true)

        
    }
    
}

