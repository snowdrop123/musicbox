//
//  MusicSearchCell.swift
//  musicBox
//
//  Created by Vadim on 14.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit

class MusicSearchCell: UITableViewCell {
    
    let musicTextLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = customTextColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = UIColor.clear
        
        addSubview(musicTextLabel)
        addSubview(separatorView)

        musicTextLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 6).isActive = true
        musicTextLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        musicTextLabel.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        musicTextLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true

        separatorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 6).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor, constant: -6).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
