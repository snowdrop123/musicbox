//
//  AddNewMusicCell.swift
//  musicBox
//
//  Created by Vadim on 15.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit

protocol AddNewMusicCellDelegate {
    func addNewMusic()
}

class AddNewMusicCell: UICollectionViewCell {
    
    var delegate: AddNewMusicCellDelegate!
    
    let contentCellView: CustomContentView = {
        let view = CustomContentView()
        view.backgroundColor = customContentDarkBlueColor
        view.layer.cornerRadius = audioCellRadius
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let audioName: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 22, weight: .semibold)
        label.numberOfLines = 2
        label.minimumScaleFactor = 0.1
        label.adjustsFontSizeToFitWidth = true
        label.textColor = customTextColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    @objc func handleTap() {
        delegate.addNewMusic()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(contentCellView)
        contentCellView.addSubview(audioName)
        
        contentCellView.isUserInteractionEnabled = true
        contentCellView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
        contentCellView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        contentCellView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        contentCellView.widthAnchor.constraint(equalToConstant: audioCellRadius * 2).isActive = true
        contentCellView.heightAnchor.constraint(equalToConstant: audioCellRadius * 2).isActive = true
        
        audioName.leftAnchor.constraint(equalTo: contentCellView.leftAnchor, constant: 0).isActive = true
        audioName.rightAnchor.constraint(equalTo: contentCellView.rightAnchor, constant: 0).isActive = true
        audioName.topAnchor.constraint(equalTo: contentCellView.topAnchor, constant: 0).isActive = true
        audioName.bottomAnchor.constraint(equalTo: contentCellView.bottomAnchor, constant: 0).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
