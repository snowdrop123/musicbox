//
//  MusicBoxCategoriestListCell.swift
//  musicBox
//
//  Created by Vadim on 16.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit

class MusicBoxCategoriestListCell: UICollectionViewCell {
    
    var contentCellView: UIView = {
        let view = UIView()
//        view.backgroundColor = customContentDarkBlueColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let categoryName: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 22, weight: .light)
        label.numberOfLines = 2
        label.minimumScaleFactor = 0.1
        label.adjustsFontSizeToFitWidth = true
        label.textColor = customTextColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        
        addSubview(contentCellView)
        contentCellView.addSubview(categoryName)
        addSubview(separatorView)

        contentCellView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        contentCellView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        contentCellView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        contentCellView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true

        categoryName.leftAnchor.constraint(equalTo: contentCellView.leftAnchor, constant: 0).isActive = true
        categoryName.rightAnchor.constraint(equalTo: contentCellView.rightAnchor, constant: 0).isActive = true
        categoryName.topAnchor.constraint(equalTo: contentCellView.topAnchor, constant: 0).isActive = true
        categoryName.bottomAnchor.constraint(equalTo: contentCellView.bottomAnchor, constant: 0).isActive = true
        
        separatorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 6).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor, constant: -6).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
