//
//  MusicBoxNewCategoryVC.swift
//  musicBox
//
//  Created by Vadim on 16.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit
import Firebase

class MusicBoxNewCategoryVC: UIViewController {
    
    lazy var textField: UITextField = {
        let tf = UITextField()
        tf.delegate = self
        tf.textColor = customTextColor
        tf.attributedPlaceholder = NSAttributedString(string: "Введите имя категории",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tf.textAlignment = .center
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = customTextColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var acceptButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(customTextColor, for: .normal)
        button.setTitle("Принять", for: .normal)
        button.addTarget(self, action: #selector(handleAccept), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    @objc func handleAccept() {
        
        if let categoryName = textField.text {
            if categoryName != "" {
                if let sessionId = UserDefaults.standard.string(forKey: sessionIDKey) {
                    Database.database().reference().child("user-categories").child(sessionId).updateChildValues(["Избранные": 1])
                    Database.database().reference().child("user-categories").child(sessionId).updateChildValues([categoryName: 1])
                    UserDefaults.standard.set(categoryName, forKey: categoryNameKey)
                    dismiss(animated: false, completion: nil)
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = customContentBlueColor
        
        navigationItem.title = "Новая категория"
        
        view.addSubview(textField)
        view.addSubview(separatorView)
        view.addSubview(acceptButton)
        
        textField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        textField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        textField.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        
        separatorView.leftAnchor.constraint(equalTo: textField.leftAnchor, constant: 0).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.rightAnchor.constraint(equalTo: textField.rightAnchor, constant: 0).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: textField.bottomAnchor, constant: 0).isActive = true

        acceptButton.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 14).isActive = true
        acceptButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        acceptButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        acceptButton.centerXAnchor.constraint(equalTo: textField.centerXAnchor, constant: 0).isActive = true

        
    }
    
}

extension MusicBoxNewCategoryVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.count == 50 && range.length == 0 && string != "" {
            return false
        }
        
        return true
    }
    
}
