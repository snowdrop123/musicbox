//
//  MusicBoxCategoriesListVC.swift
//  musicBox
//
//  Created by Vadim on 16.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit
import Firebase

class MusicBoxCategoriesListVC: UIViewController {
    
    let cellId = "cellId"
    var fromCategoryName = ""
    
    let collectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
        
    }()
    
    var categories = [String]()
    
    @objc func handleBack() {
        dismiss(animated: false, completion: nil)
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        
        if gesture.state != .began {
            return
        }
        
        let p = gesture.location(in: collectionView)
        
        if let indexPath = collectionView.indexPathForItem(at: p) {
            
            if indexPath.item < categories.count {
                
                if (categories[indexPath.item] == "Избранные") || (categories[indexPath.item] == fromCategoryName) {
                    
                    
                } else {
                    
                    let alertController = UIAlertController(title: "Опции", message: nil, preferredStyle: .actionSheet)
                    alertController.addAction(UIAlertAction(title: "Удалить", style: .default, handler: { (action) in
                        
                        self.deleteCategoryFromMyList(itemIndex: indexPath.item)
                        
                    }))
                    
                    alertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                    present(alertController, animated: true, completion: nil)
                    
                }
            }
            
            
        } else {
            print("couldn't find index path")
        }
    }
    
    func deleteCategoryFromMyList(itemIndex: Int) {
        
        if let sessionId = UserDefaults.standard.string(forKey: sessionIDKey) {
            
            Database.database().reference().child("user-categories").child(sessionId).child(categories[itemIndex]).removeValue()
            Database.database().reference().child("playlists").child(sessionId).child(categories[itemIndex]).removeValue()
            categories.remove(at: itemIndex)
            collectionView.reloadData()
            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = customContentBlueColor
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: self, action: #selector(handleBack))
        
        navigationItem.title = "Категории"
        
        view.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MusicBoxCategoriestListCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true

        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        collectionView.addGestureRecognizer(lpgr)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchMusicList()
    }
    
    func fetchMusicList() {
        

        if let sessionId = UserDefaults.standard.string(forKey: sessionIDKey) {
            
            categories = []
            collectionView.reloadData()
            
            Database.database().reference().child("user-categories").child(sessionId).observeSingleEvent(of: .value) { (snapshot) in
                
                guard let dictionary = snapshot.value as? [String: AnyObject] else {
                    return
                }
                
                var count = 0
                
                for key in dictionary.keys {
                    
                    self.categories.append(key)
                    
                    count += 1
                    
                    if count == dictionary.keys.count {
                        self.collectionView.reloadData()
                    }
                    
                }
                
            }
            
        }
        
    }

    
}

extension MusicBoxCategoriesListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if categories.count == 0 {
            return 2
        } else {
            return 1 + categories.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MusicBoxCategoriestListCell
        
        if categories.count == 0 {
            if indexPath.item == 0 {
                cell.categoryName.text = "Избранные"
            } else {
                cell.categoryName.text = "+\nДобавить"
            }
        } else {
            if indexPath.item == categories.count {
                cell.categoryName.text = "+\nДобавить"
            } else {
                cell.categoryName.text = categories[indexPath.item]
            }

        }
        
        return cell
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 200)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if categories.count == 0 {
            if indexPath.item == 1 {
                let nextVC = MusicBoxNewCategoryVC()
                navigationController?.pushViewController(nextVC, animated: true)
            } else {
                
                UserDefaults.standard.set("Избранные", forKey: categoryNameKey)
                dismiss(animated: false, completion: nil)
                
            }
        } else {
            
            if indexPath.item == categories.count {
                
                if categories.count == 9 {
                    let alertController = UIAlertController(title: "Количество категорий не может превышать 9-ти", message: "Зажмите категорию для редактирования", preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
                    
                    present(alertController, animated: true, completion: nil)
                } else {
                    
                    if Reachability.isConnectedToNetwork() {
                        
                        let nextVC = MusicBoxNewCategoryVC()
                        navigationController?.pushViewController(nextVC, animated: true)

                    } else {
                        
                        let alertController = UIAlertController(title: title, message: "Проверьте соединение к интернету", preferredStyle: .alert)
                        
                        
                        alertController.addAction(UIAlertAction(title: "ОК", style: .cancel) { (action) in
                            
                        })
                        
                        present(alertController, animated: true, completion: nil)
                        
                    }

                }
                
            } else {
                
                UserDefaults.standard.set(categories[indexPath.item], forKey: categoryNameKey)
                dismiss(animated: false, completion: nil)

            }
            
        }
        
    }
    
}
