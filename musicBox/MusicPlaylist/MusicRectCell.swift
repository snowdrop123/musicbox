//
//  MusicRectCell.swift
//  musicBox
//
//  Created by Vadim on 14.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit

protocol MusicRectCellDelegate {
    func musicRectCellDelegatePressed(indexRow: Int)
    func musicRectCellDelegateLongPressed(indexRow: Int)
}

class CustomContentView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    
    var touchPath: UIBezierPath {return UIBezierPath(ovalIn: self.bounds)}
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return touchPath.contains(point)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MusicRectCell: UICollectionViewCell {
    
    var audio: Audio? {
        didSet {
            
            if let audioNameText = audio?.audioName {
                audioName.text = audioNameText
            } else {
                audioName.text = ""
            }
            
        }
    }
        
    var indexRow = 0
    
    var delegate: MusicRectCellDelegate!
    
    let shapeLayer = CAShapeLayer()
//    let trackLayer = CAShapeLayer()
    
    var contentCellView: CustomContentView = {
        let view = CustomContentView()
        view.backgroundColor = customContentDarkBlueColor
        view.layer.cornerRadius = audioCellRadius
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let audioName: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 22, weight: .light)
        label.numberOfLines = 2
        label.minimumScaleFactor = 0.1
        label.adjustsFontSizeToFitWidth = true
        label.textColor = customTextColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var audioStatusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isHidden = true
        imageView.tintColor = customTextColor
        let image = UIImage(named: "pause-button")?.withRenderingMode(.alwaysTemplate)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    @objc func handleTap() {
        delegate.musicRectCellDelegatePressed(indexRow: indexRow)
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        
        if gesture.state != .began {
            return
        }
        
        delegate.musicRectCellDelegateLongPressed(indexRow: indexRow)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(contentCellView)
        contentCellView.addSubview(audioName)
        contentCellView.addSubview(audioStatusImageView)
        
        contentCellView.isUserInteractionEnabled = true
        contentCellView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        contentCellView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress)))
        
        contentCellView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        contentCellView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        contentCellView.widthAnchor.constraint(equalToConstant: audioCellRadius * 2).isActive = true
        contentCellView.heightAnchor.constraint(equalToConstant: audioCellRadius * 2).isActive = true
        
        audioName.leftAnchor.constraint(equalTo: contentCellView.leftAnchor, constant: 0).isActive = true
        audioName.rightAnchor.constraint(equalTo: contentCellView.rightAnchor, constant: 0).isActive = true
        audioName.topAnchor.constraint(equalTo: contentCellView.topAnchor, constant: 0).isActive = true
        audioName.bottomAnchor.constraint(equalTo: contentCellView.bottomAnchor, constant: 0).isActive = true
        
        audioStatusImageView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        audioStatusImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        audioStatusImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        audioStatusImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let rowsCount = Int(UIScreen.main.bounds.width / 180)
        let cellCenter = CGPoint(x: UIScreen.main.bounds.width / CGFloat(2 * rowsCount), y: 100)
        
        let circularPath = UIBezierPath(arcCenter: cellCenter, radius: audioCellRadius, startAngle: -0.5 * CGFloat.pi, endAngle: 1.5 * CGFloat.pi, clockwise: true)
//        trackLayer.path = circularPath.cgPath
//
//        trackLayer.strokeColor = UIColor.clear.cgColor
//        trackLayer.lineWidth = 4
//        trackLayer.fillColor = UIColor.clear.cgColor
//        trackLayer.lineCap = CAShapeLayerLineCap.round
//        layer.addSublayer(trackLayer)
        
        shapeLayer.path = circularPath.cgPath
        
        shapeLayer.strokeColor = customTextColor.cgColor
        shapeLayer.lineWidth = 4
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        
        shapeLayer.strokeEnd = 0
        
        layer.addSublayer(shapeLayer)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
