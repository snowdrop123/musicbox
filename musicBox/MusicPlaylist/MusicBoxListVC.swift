//
//  MusicBoxListVC.swift
//  musicBox
//
//  Created by Vadim on 14.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

class MusicBoxListVC: UIViewController {
    
    let cellId = "cellId"
    let cellId2 = "cellId2"
    
    var categoryName = "Избранные"
    
    var needToReloadCell = false
    var indexToReloadCell = 0
    
    let collectionView: UICollectionView = {
       
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.bounces = true
        cv.alwaysBounceVertical = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
        
    }()
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        
        if gesture.state != .began {
            return
        }
        
        let p = gesture.location(in: collectionView)
        
        if let indexPath = collectionView.indexPathForItem(at: p) {
            
            if indexPath.item < myList.count {
                let alertController = UIAlertController(title: "Опции", message: nil, preferredStyle: .actionSheet)
                alertController.addAction(UIAlertAction(title: "Удалить", style: .default, handler: { (action) in
                    
                    self.deleteMusicFromMyList(itemIndex: indexPath.item)
                    
                }))
                
                alertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                present(alertController, animated: true, completion: nil)
            }
            
            
        } else {
            print("couldn't find index path")
        }
    }
    
    func deleteMusicFromMyList(itemIndex: Int) {
        
        if let sessionId = UserDefaults.standard.string(forKey: sessionIDKey), let audioId = myList[itemIndex].id {
            
            Database.database().reference().child("playlists").child(sessionId).child(categoryName).child(audioId).removeValue()
            
            myList.remove(at: itemIndex)
            collectionView.reloadData()
            
        }
        
    }

    @objc func clickOnButton() {
        
        let nextVC = MusicBoxCategoriesListVC()
        
        if let categoryNameFromUserDefaults = UserDefaults.standard.string(forKey: categoryNameKey) {
            nextVC.fromCategoryName = categoryNameFromUserDefaults
        } else {
            nextVC.fromCategoryName = "Избранные"
        }
        
        let navigationNextVC = UINavigationController(rootViewController: nextVC)
        present(navigationNextVC, animated: true, completion: nil)
        
    }
    
//    var navigationTitleButton = UIButton()
//    var navigationTitleLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
//        navigationController?.navigationItem.largeTitleDisplayMode = .never
        
//        navigationTitleButton = UIButton(type: .custom)
//        navigationTitleButton.frame = CGRect(x: 0, y: 0, width: 120, height: 40)
//        navigationTitleButton.backgroundColor = UIColor.clear
//        let image = UIImage(named: "expand-button")?.withRenderingMode(.alwaysTemplate)
//        navigationTitleButton.imageView?.tintColor = UIColor.white
//        navigationTitleButton.imageView?.contentMode = .scaleAspectFit
//        navigationTitleButton.setImage(image, for: .normal)
//        navigationTitleButton.imageEdgeInsets = UIEdgeInsetsMake(15, 106, 15, 4);
//        navigationTitleButton.addTarget(self, action: #selector(clickOnButton), for: .touchUpInside)
//
//        navigationTitleLabel = UILabel(frame: CGRect(x: 3, y: 5, width: 100, height: 30))
//        navigationTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .light)
//        navigationTitleLabel.text = "Плейлист"
//        navigationTitleLabel.textAlignment = .center
//        navigationTitleLabel.textColor = UIColor.white
//        navigationTitleLabel.minimumScaleFactor = 0.1
//        navigationTitleLabel.adjustsFontSizeToFitWidth = true
//        navigationTitleLabel.backgroundColor =   UIColor.clear
//        navigationTitleButton.addSubview(navigationTitleLabel)
        
//        navigationItem.titleView = navigationTitleButton
        
        navigationItem.title = "Избранные"
        
        extendedLayoutIncludesOpaqueBars = true
        navigationController?.navigationBar.isTranslucent = true

        view.backgroundColor = UIColor.orange
        
        view.backgroundColor = customContentBlueColor
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(checkIfSomeAudioIsRunning), name: UIApplication.willResignActiveNotification, object: nil)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MusicRectCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(AddNewMusicCell.self, forCellWithReuseIdentifier: cellId2)
        
        view.addSubview(collectionView)
        
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
    }
    
    @objc func checkIfSomeAudioIsRunning() {
        if someAudioIsRunning {
            
            someAudioIsRunning = false
            audioPlayer.pause()
            
            collectionView.reloadItems(at: [IndexPath(item: someAudioRunningIndex, section: 0)])
            
        }
    }
    
    var myList = [Audio]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchMyList()
    }

    func fetchMyList() {
        
        myList = []
        
        if let sessionId = UserDefaults.standard.string(forKey: sessionIDKey) {

            if let categoryNameFromUserDefaults = UserDefaults.standard.string(forKey: categoryNameKey) {

                categoryName = categoryNameFromUserDefaults
            } else {
                categoryName = "Избранные"
                UserDefaults.standard.set("Избранные", forKey: categoryNameKey)
            }
            
//            navigationTitleLabel.text = categoryName
        Database.database().reference().child("playlists").child(sessionId).child(categoryName).observeSingleEvent(of: .value) { (snapshot) in
                
                guard let dictionary = snapshot.value as? [String: AnyObject] else {
                    self.collectionView.reloadData()

                    return
                }
            
                for key in dictionary.keys {
                    Database.database().reference().child("music").child(key).observeSingleEvent(of: .value, with: { (snapshot2) in
                        
                        guard let dictionary2 = snapshot2.value as? [String: AnyObject] else {
                            return
                        }
                        
                        let music = Audio(dictionary: dictionary2)
                        music.id = key

                        self.myList.append(music)
                        
                        if self.myList.count == dictionary.keys.count {
                            self.collectionView.reloadData()
                        }

                        
                    }, withCancel: nil)
                    
                    
                }
                
            }

            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        checkIfSomeAudioIsRunning()
        
    }
    
    var audioPlayer: AVAudioPlayer!
    
    var someAudioIsRunning = false
    var someAudioRunningIndex = 0
    
    func downloadFile(url: String) {
        
        //LoadingOverlay alert
        //call dismiss to hide this
        let alert = UIAlertController(title: nil, message: "Загрузка аудио...", preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        if let audioUrl = URL(string: url) {

            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)

            if FileManager.default.fileExists(atPath: destinationUrl.path) {

                dismiss(animated: false, completion: nil)

                let alertController = UIAlertController(title: "Ошибка", message: "Аудио уже скачано", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
                present(alertController, animated: true, completion: nil)

            } else {

                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    
                    if error != nil {
                        
                        self.dismiss(animated: false, completion: nil)

                        let alertController = UIAlertController(title: nil, message: "Возникла ошибка при скачивании", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)

                        
                        return
                    }
                    
                    guard let location = location, error == nil else { return }
                    do {

                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        
                        self.dismiss(animated: false, completion: nil)
                        
                        let alertController = UIAlertController(title: nil, message: "Аудио сохранено", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                        
                    } catch _ as NSError {
                        
                        self.dismiss(animated: false, completion: nil)

                        let alertController = UIAlertController(title: nil, message: "Возникла ошибка при скачивании", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)

                    }
                }).resume()
                
            }
        }
        
    }
    
    func playDownloaded(indexRow: Int) {
        
        if let audioUrl = myList[indexRow].audioUrl, let url = URL(string: audioUrl) {

            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(url.lastPathComponent)
            
            if FileManager.default.fileExists(atPath: destinationUrl.path) {

                if someAudioIsRunning == false {
                    
                    someAudioRunningIndex = indexRow
                    someAudioIsRunning = true

                    do {
                        audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                        guard let player = audioPlayer else { return }
                        player.delegate = self
                        player.prepareToPlay()
                        player.play()
                        
                    } catch let error {
                        print(error.localizedDescription)
                    }
                    
                    let selectedCell = collectionView.cellForItem(at: IndexPath(item: indexRow, section: 0)) as! MusicRectCell
                    selectedCell.audioName.isHidden = true
                    selectedCell.audioStatusImageView.isHidden = false
                    
                    let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
                    
                    basicAnimation.toValue = 1
                    basicAnimation.duration = audioPlayer.duration
                    
                    basicAnimation.fillMode = CAMediaTimingFillMode.forwards
                    basicAnimation.isRemovedOnCompletion = false
                    
                    selectedCell.shapeLayer.isHidden = false
                    
                    selectedCell.shapeLayer.add(basicAnimation, forKey: "urSoBasic")
                    
                }

                
            } else {
                
                downloadFile(url: audioUrl)
                
            }
            
        }
    }

    
}


extension MusicBoxListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return myList.count + 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item < myList.count {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MusicRectCell
            
            cell.delegate = self
            cell.indexRow = indexPath.item
            cell.audio = myList[indexPath.row]
            
            cell.shapeLayer.isHidden = true
            cell.audioName.isHidden = false
            cell.audioStatusImageView.isHidden = true
            
            cell.shapeLayer.removeAllAnimations()
            
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId2, for: indexPath) as! AddNewMusicCell

            cell.delegate = self
            cell.audioName.text = "+\nДобавить"
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let orientation = UIApplication.shared.statusBarOrientation
        
        if (orientation == .landscapeLeft || orientation == .landscapeRight) {
            let rowsCount = Int(view.frame.width / 180)
            
            return CGSize(width: UIScreen.main.bounds.size.width / CGFloat(rowsCount), height: 200)
        } else {
            let rowsCount = Int(view.frame.width / 180)
                        
            return CGSize(width: UIScreen.main.bounds.size.width / CGFloat(rowsCount), height: 200)

        }
        
    }
    
}

extension MusicBoxListVC: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        someAudioIsRunning = false
        
        collectionView.reloadItems(at: [IndexPath(item: someAudioRunningIndex, section: 0)])

    }
    
}

extension MusicBoxListVC: MusicRectCellDelegate {
    
    func musicRectCellDelegatePressed(indexRow: Int) {
        
        if someAudioIsRunning {
            
            someAudioIsRunning = false
            audioPlayer.pause()
            
            collectionView.reloadItems(at: [IndexPath(item: someAudioRunningIndex, section: 0)])
            
            if someAudioRunningIndex != indexRow {
                
                playDownloaded(indexRow: indexRow)
                
            }
            
        } else {
            playDownloaded(indexRow: indexRow)
        }
        
        
    }
    
    func musicRectCellDelegateLongPressed(indexRow: Int) {
        
        let alertController = UIAlertController(title: "Опции", message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Удалить", style: .default, handler: { (action) in
            
            self.deleteMusicFromMyList(itemIndex: indexRow)
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
}

extension MusicBoxListVC: AddNewMusicCellDelegate {
    
    func addNewMusic() {
        
        if myList.count == 9 {
            let alertController = UIAlertController(title: "Количество аудиодорожек не может превышать 9-ти", message: "Зажмите аудиодорожку для редактирования", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
            
            present(alertController, animated: true, completion: nil)
        } else {
            
            checkIfSomeAudioIsRunning()
            
            if Reachability.isConnectedToNetwork() {

                let nextVC = MusicCategoriesSearchVC()
//                let nextVC = TestView()
                nextVC.categoryName = categoryName
                navigationController?.pushViewController(nextVC, animated: true)
            } else {
                let alertController = UIAlertController(title: title, message: "Проверьте соединение к интернету", preferredStyle: .alert)
                
                alertController.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
                
                present(alertController, animated: true, completion: nil)
            }
        }
        
        
    }
    
    
}

