//
//  Constants.swift
//  musicBox
//
//  Created by Vadim on 14.09.2018.
//  Copyright © 2018 ld. All rights reserved.
//

import UIKit

let sessionIDKey = "sessionIDKey"
let categoryNameKey = "categoryNameKey"

let customBackgroundBlueColor = UIColor.init(red: 29/255, green: 39/255, blue: 68/255, alpha: 1)
let customContentBlueColor = UIColor.init(red: 43/255, green: 67/255, blue: 101/255, alpha: 1)
let customContentDarkBlueColor = UIColor.init(red: 38/255, green: 59/255, blue: 89/255, alpha: 1)
let customTextColor = UIColor.white

let audioCellRadius: CGFloat = 85
